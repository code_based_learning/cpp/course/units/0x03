// (C) 2024 A.Voß, a.voss@fh-aachen.de, cpp@codebasedlearning.dev

#include <iostream>
using std::cout;
using std::endl;
using std::ostream;
using std::string;
using std::array;

// auto red = "\033[1;31m";
// auto green = "\033[1;32m";
// auto back = "\033[0m";

class Color {
public:
    inline static const std::string red = "\033[1;31m";
    inline static const std::string green = "\033[1;32m";
    inline static const std::string yellow = "\033[1;33m";
    inline static const std::string blue = "\033[1;34m";
    inline static const std::string magenta = "\033[1;35m";
    inline static const std::string cyan = "\033[1;36m";
    inline static const std::string white = "\033[1;37m";
    inline static const std::string reset = "\033[0m";

    //inline static ostream&
};

class ColoredText {
public:
    ColoredText(const std::string& text, const std::string& color) : text_(text), color_(color) {}

    friend std::ostream& operator<<(std::ostream& os, const ColoredText& coloredText) {
        os << coloredText.color_ << coloredText.text_ << Color::reset;
        return os;
    }

private:
    std::string text_;
    std::string color_;
};

ColoredText colored(const std::string& text, const std::string& color) {
    return ColoredText(text, color);
}

class street {
public:
    string name;
    int number;

    street(const string& name, int number) : name{name}, number{number} {
        cout << " a| ... " << Color::red << "[street-ctor]" << Color::reset << " " << name << " " << number << endl;
    }
    ~street() {
        cout << " b| ... " << colored("[street-dtor]",Color::green) << " " << name << " " << number << endl;
    }
};

class address {
public:
    int code;
    street street;

    address(const string& name, int number, int code, bool do_throw = false) : code{code}, street{name,number} {                               // (C)
        cout << " A| . [address-ctor] " << code << endl;
        if (do_throw) throw std::runtime_error("Throwing from address-ctor");
    }
    ~address() {
        cout << " B| . [address-dtor] " << code << endl;
    }
};

int main()
{
    cout << endl << "--- " << __FILE__ << " ---" << endl;

    // Case 1: Aufrufe der ctors und dtors; insbes. street vor address beim Erzeugen und nach address beim Zerstören
    cout << endl << " 1| Case 1" << endl;
    {
        address a("Seffenter Weg",23,52078);
    }

    // Case 2: Aufruf address-ctor mit Exception; man sieht, dass a1 und a2.street erzeugt und zerstört werden
    //         und a2 nie existierte
    cout << endl << " 2| Case 2" << endl;
    try
    {
        address a1("Seffenter Weg",23,52078);
        address a2("Haarener Str.",42,52064,true);
        cout << " .| never..." << endl;
    }
    catch (const std::runtime_error& e) {
        cout << "--| Exception: " << e.what() << endl;
    }

    // Case 3 (einkommentieren): bei Exception in main gibt es keinen Aufräumschritt mehr (Sondersituation)
    cout << endl << " 3| Case 3" << endl;
    // address a("Seffenter Weg",23,52078,true);

    // Case 4 (einkommentieren): bei Exceptions im dtor gibt es keine Möglichkeit, diese zu fangen
    cout << endl << " 4| Case 4" << endl;
    class DestroyMe {
    public:
        ~DestroyMe() {
            // throw std::runtime_error("Throwing from DestroyMe-dtor");
            cout << " C| . [DestroyMe-dtor] " << endl;
        }
    };
    try {
        DestroyMe d;
    } catch (const std::runtime_error& e) {
        cout << "--| Exception: " << e.what() << endl;
    }

    cout << endl << "--- " << __FILE__ << " ---" << endl << endl;
    return 0;
}
